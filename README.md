# IDS 721 Mini Project 1 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-1/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-1/-/commits/main)
## Link to Website
https://ids-721-mini-proj-1-jaxonyue-5372175765c7fe630bff8b61637b439e45.gitlab.io/

## Screenshot of Portfolio Page
![Screenshot_2024-01-29_at_10.49.53_AM](/uploads/0147ba9dde7a31d5f0dd54f82be45e10/Screenshot_2024-01-29_at_10.49.53_AM.png)

## Overview
* This repository includes the components for **Mini-Project 1 - Create a Static Website with Zola**

## Goal
* Create a static website using **Zola**, a Rust static site generator
* Use the site to hold all of the portfolio work in IDS 721
* Build a home page and portfolio page using Zola's templates
* Use CSS to style the site

## Key Steps
* Clone the repository
* Install Zola using the instructions here: https://www.getzola.org/documentation/getting-started/installation/
* Install your theme by choosing one from the Zola theme gallery: https://www.getzola.org/themes/, and then following the instructions for that theme. I chose the Hephaestus theme.
* Customize the theme to fit the needs of the site. I added two sections: "Education" and "Projects". In the "Education" section, I added my undergraduate and graduate education. In the "Projects" section, I added the links to my Gitlab repositories for the mini projects in IDS 721.
* Ensure that the runner can access the theme by adding the following to the .gitmodules file:
```
[submodule "themes/hephaestus"]
	path = themes/hephaestus
	url = https://github.com/BConquest/hephaestus.git
```
* Set up the GitLab CI/CD pipeline to build the site and deploy it to GitLab pages. I used the following .gitlab-ci.yml file:
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
* Push the changes to the repository and check the GitLab CI/CD pipeline to ensure that the site is built and deployed to GitLab pages.
* On the left sidebar of GitLab, navigate to **Deploy > Pages** to find the link to the site.
